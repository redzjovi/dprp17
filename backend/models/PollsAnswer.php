<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "polls_answer".
 *
 * @property integer $id
 * @property integer $id_poll
 * @property string $answer
 */
class PollsAnswer extends \yii\db\ActiveRecord
{

    public $laravel;
    public $yii2;
    public $CI;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'polls_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'answer'], 'required'],
            ['id', 'integer'],
            [['answer'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'answer' => 'Answer',
        ];
    }
}
