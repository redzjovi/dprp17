<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\PollsAnswer */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="polls-form">

    <?php $form = ActiveForm::begin(); ?>
    
       <label class="control-label" for="modelname-attributename"><h3>Apa Framework yang anda sukai?</h3></label>
    
    <br>
    <?= $form->field($model, 'answer')->radioList( 
    					[
    						'0'=>'Laravel', 
    						'1' => 'yii2', 
    						'2' => 'CI'
    					],
    					['separator' => '<br>']
    						 );?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
